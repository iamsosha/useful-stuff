﻿using System;

class Calc
{
    public static float Sum(float a, float b) => a + b;
    public static float AntiSum(float a, float b) => a - b;
    public static float Mult(float a, float b) => a * b;
    public static float Dev(float a, float b)
    {
        if (b != 0)
            return a / b;
        else
        {
            Console.WriteLine("You cannot divide by 0");
            return 0;
        }
    }

}

class Program
{
    static void Main(string[] args)
    {
        while (true)
        {
            try
            {


                var input = Console.ReadLine();
                if (input == "exit")
                    break;
                var array = input.Split(' ');
                switch (array[1])
                {
                    case "+":
                        Console.WriteLine(Calc.Sum(float.Parse(array[0]), float.Parse(array[2])));
                        break;
                    case "-":
                        Console.WriteLine(Calc.AntiSum(float.Parse(array[0]), float.Parse(array[2])));
                        break;
                    case "*":
                        Console.WriteLine(Calc.Mult(float.Parse(array[0]), float.Parse(array[2])));
                        break;
                    case "/":
                        Console.WriteLine(Calc.Dev(float.Parse(array[0]), float.Parse(array[2])));
                        break;
                    default:
                        Console.WriteLine("Some shit found");
                        break;
                }
            }
            catch
            {
                Console.WriteLine("Some shit found");
                Console.WriteLine("Expected expression as: {number} operator {number}");
            }
        }
    }
}

